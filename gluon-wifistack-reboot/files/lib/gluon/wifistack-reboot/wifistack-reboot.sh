#!/bin/sh
set -e
set -x

UPTIME_NOW=$(cut -d '.' -f1 /proc/uptime)
UPTIME_MIN=180

# Run reboot
if [ -f /tmp/autoupdate.lock ]
then
    touch /tmp/schedule_reboot
else
    # Only reboot if $UPTIME_NOW > $UPTIME_MIN
    if test $UPTIME_NOW -lt $UPTIME_MIN
    then
        touch /tmp/schedule_reboot
    else
        reboot
    fi
fi

exit
