#!/bin/sh
set -e
set -x

. /usr/share/libubox/jshn.sh

UPTIME_NOW=$(cut -d '.' -f1 /proc/uptime)
UPTIME_MIN=180

# WORKAROUND: Disable multicast optimization
batctl mm 0 || true

# Wait for up to 99 seconds until execution
if [ "$1" != "now" ]
then
    sleep $(head -30 /dev/urandom | tr -dc "0123456789" | head -c2)
fi

uci_hash=`/sbin/uci show | md5sum | awk '{ print $1 }'`

schedule_reboot="0"

if [ -f /tmp/schedule_reboot ]
then
    schedule_reboot="1"
fi

hwid=$(cat /lib/gluon/core/sysconfig/primary_mac | sed 's/\://g')

router_model=`lua -e 'print(require("platform_info").get_image_name())'`

if [ -f "/lib/gluon/core/sysconfig/single_ifname" ]
then
    lan_if=`cat /lib/gluon/core/sysconfig/single_ifname`
else
    lan_if=`cat /lib/gluon/core/sysconfig/wan_ifname`
fi

node_info=$(/bin/uclient-fetch "https://wifi-api.managed-infra.com/api/v2/wirelessnode/${hwid}" -q -O -)

json_load "${node_info}"
json_get_var node_hostname_fqdn node_hostname_fqdn
json_get_var geolat wireless_node_geo_lat
json_get_var geolong wireless_node_geo_long
json_get_var lldp_state wireless_node_lldp_enabled
json_get_var snmp_state wireless_node_snmp_enabled
json_get_var captive_portal_state wireless_node_captive_portal_enabled
json_get_var updater_state wireless_node_updater_enabled
json_get_var updater_branch wireless_node_updater_branch
json_get_var mesh_on_lan wireless_node_mesh_on_lan_enabled
json_get_var lan_bridge wireless_node_lan_bridge
json_get_var domain wireless_node_domain
json_get_var traffic_shaping_enabled wireless_node_traffic_shaping_enabled
json_get_var traffic_shaping_egress_kbps wireless_node_traffic_shaping_egress_kbps
json_get_var traffic_shaping_ingress_kbps wireless_node_traffic_shaping_ingress_kbps
json_get_var syslog_receiver_ip monitoring_syslog_receiver_01_ip
json_get_var syslog_receiver_port monitoring_syslog_receiver_01_port
json_get_var syslog_snmp_trap_ip monitoring_snmp_trap_receiver_01_ip
json_get_var syslog_snmp_trap_port monitoring_snmp_trap_receiver_01_port

# Check and switch domain
node_current_domain=$(uci get gluon.core.domain)

if [ "$node_current_domain" != "$domain" ]
then
    uci set gluon.core.domain="$domain"
    gluon-reconfigure || true
fi

# set correct router name
uci set system.@system[0].hostname="dc-$hwid.nodes.managed-infra.com"
uci set system.@system[0].pretty_hostname="dc-$hwid.nodes.managed-infra.com"

uci set system.@system[0].log_remote='1'
uci set system.@system[0].log_proto='udp'
uci set system.@system[0].log_ip="$syslog_receiver_ip"
uci set system.@system[0].log_port="$syslog_receiver_port"
uci set system.@system[0].conloglevel=7

uci commit system || true

uci get gluon-node-info.@location[0] || uci add gluon-node-info location
uci set gluon-node-info.@location[0]='location'
uci set gluon-node-info.@location[0].share_location='1'
uci set gluon-node-info.@location[0].latitude="$geolat"
uci set gluon-node-info.@location[0].longitude="$geolong"
uci commit gluon-node-info || true

uci get gluon-node-info.@owner[0] || uci add gluon-node-info owner
uci set gluon-node-info.@owner[0].contact=noc@dolphin-connect.com
uci commit gluon-node-info || true

#if [ "$mesh_on_lan" = "1" ]
#then
#    # activate mesh
#    uci set network.mesh_wan.disabled=0
#    uci commit network
#else
#    # deactivate mesh
#    uci set network.mesh_wan.disabled=1
#    uci commit network
#fi

if [ "$traffic_shaping_enabled" = "1" ]
then
    # activate traffic shaping
    uci set simple-tc.mesh_vpn='interface'
    uci set simple-tc.mesh_vpn.ifname='mesh-vpn'
    uci set simple-tc.mesh_vpn.enabled='1'
    uci set simple-tc.mesh_vpn.limit_ingress="$traffic_shaping_egress_kbps"
    uci set simple-tc.mesh_vpn.limit_egress="$traffic_shaping_ingress_kbps"
    uci commit simple-tc || true
else
    # deactivate traffic shaping
    uci set simple-tc.mesh_vpn='interface'
    uci set simple-tc.mesh_vpn.ifname='mesh-vpn'
    uci set simple-tc.mesh_vpn.enabled='0'
    uci set simple-tc.mesh_vpn.limit_ingress="$traffic_shaping_egress_kbps"
    uci set simple-tc.mesh_vpn.limit_egress="$traffic_shaping_ingress_kbps"
    uci commit simple-tc || true
fi

if [ "$lldp_state" = "1" ]
then
    if [ -f "/etc/config/lldpd" ]
    then
        # activate LLDP
        echo 16384 > /sys/class/net/br-client/bridge/group_fwd_mask
        echo 16384 > /sys/class/net/br-wan/bridge/group_fwd_mask
        
        uci set lldpd.config.enable_cdp=1
        uci set lldpd.config.enable_fdp=1
        uci set lldpd.config.enable_sonmp=1
        uci set lldpd.config.enable_edp=1

        uci delete lldpd.config.interface || true
        uci add_list lldpd.config.interface='*'
        
        uci commit lldpd || true

        /etc/init.d/lldpd enable
        /etc/init.d/lldpd reload
    else
        echo "LLDP config not found."
    fi
else
    if [ -f "/etc/config/lldpd" ]
    then
        # disable LLDP
        echo 0 > /sys/class/net/br-client/bridge/group_fwd_mask
        echo 0 > /sys/class/net/br-wan/bridge/group_fwd_mask

        uci delete lldpd.config.interface || true
        uci commit lldpd || true

        /etc/init.d/lldpd disable
        /etc/init.d/lldpd stop
    else
        echo "LLDP config not found."
    fi
fi

if [ "$snmp_state" = "1" ]
then
    if [ -f "/etc/config/snmpd" ]
    then
        # activate SNMP
        uci set firewall.wan_snmp=rule
        uci set firewall.wan_snmp.dest_port='161'
        uci set firewall.wan_snmp.src='wan'
        uci set firewall.wan_snmp.name='wan_snmp'
        uci set firewall.wan_snmp.target='ACCEPT'
        uci set firewall.wan_snmp.proto='udp'
        uci set firewall.local_client_snmp=rule
        uci set firewall.local_client_snmp.dest_port='161'
        uci set firewall.local_client_snmp.src='local_client'
        uci set firewall.local_client_snmp.name='local_client_snmp'
        uci set firewall.local_client_snmp.target='ACCEPT'
        uci set firewall.local_client_snmp.proto='udp'
        uci set firewall.mesh_snmp=rule
        uci set firewall.mesh_snmp.dest_port='161'
        uci set firewall.mesh_snmp.src='mesh'
        uci set firewall.mesh_snmp.name='mesh_snmp'
        uci set firewall.mesh_snmp.target='ACCEPT'
        uci set firewall.mesh_snmp.proto='udp'
        
        uci commit firewall || true
        
        uci set snmpd.@system[0]=system
        uci set snmpd.@system[0].sysLocation='Office'
        uci set snmpd.@system[0].sysContact='noc@dolphin-connect.com'
        uci set snmpd.@system[0].sysName="dc-$hwid.nodes.managed-infra.com"
        
        uci get snmpd.@trap2sink[0] || uci add snmpd trap2sink
        uci set snmpd.@trap2sink[0]=trap2sink
        uci set snmpd.@trap2sink[0].host="$syslog_snmp_trap_ip"
        uci set snmpd.@trap2sink[0].community='public'
        uci set snmpd.@trap2sink[0].port="$syslog_snmp_trap_port"
        
        uci commit snmpd || true

        /etc/init.d/snmpd enable
        /etc/init.d/snmpd restart
    else
        echo "SNMP config not found."
    fi
else
    if [ -f "/etc/config/snmpd" ]
    then
        # disable SNMP
        uci del firewall.wan_snmp || true
        uci del firewall.local_client_snmp || true
        uci del firewall.mesh_snmp || true
        
        uci commit firewall || true

        /etc/init.d/snmpd disable || true
        /etc/init.d/snmpd stop || true
    else
        echo "SNMP config not found."
    fi
fi

if [ "$updater_state" = "1" ]
then
    # activate autoupdater
    uci set autoupdater.settings.enabled=1
    uci set autoupdater.settings.branch=$updater_branch
    uci commit autoupdater
else
    # deactivate autoupdater
    uci set autoupdater.settings.enabled=0
    uci set autoupdater.settings.branch=$updater_branch
    uci commit autoupdater
fi

if [ ! -f "/lib/gluon/core/sysconfig/single_ifname" ]
then
    if [ "$lan_bridge" = "1" ]
    then
        # enable lan bridge
        # MOL can not use the same interface
        uci delete network.mesh_lan.ifname || true
        uci set network.mesh_lan.disabled=1 || true
        
        # Clear WAN interfaces
        uci delete network.wan.ifname || true
        
        if [ -f "/lib/gluon/core/sysconfig/single_ifname" ]
        then
            for ifname in $(cat /lib/gluon/core/sysconfig/single_ifname); do
                uci add_list network.wan.ifname=$ifname
                uci del_list network.client.ifname=$ifname || true
            done
        else
            for ifname in $(cat /lib/gluon/core/sysconfig/wan_ifname); do
                uci add_list network.wan.ifname=$ifname
                uci del_list network.client.ifname=$ifname || true
            done
            for ifname in $(cat /lib/gluon/core/sysconfig/lan_ifname); do
                uci add_list network.wan.ifname=$ifname
                uci del_list network.client.ifname=$ifname || true
            done
        fi
        
        uci commit network
    else
        # TODO: disable lan bridge
        uci delete network.wan.ifname || true
        
        if [ -f "/lib/gluon/core/sysconfig/single_ifname" ]
        then
            for ifname in $(cat /lib/gluon/core/sysconfig/single_ifname); do
                uci add_list network.wan.ifname=$ifname
                uci del_list network.client.ifname=$ifname || true
            done
        else
            for ifname in $(cat /lib/gluon/core/sysconfig/wan_ifname); do
                uci add_list network.wan.ifname=$ifname
                uci del_list network.client.ifname=$ifname || true
            done
            
            for ifname in $(cat /lib/gluon/core/sysconfig/lan_ifname); do
                # Make sure that if is not duplicated
                uci del_list network.client.ifname=$ifname
                uci add_list network.client.ifname=$ifname || true
            done
        fi

        uci commit network
    fi
fi

# CLEANUP VLANS
KNOWN_VLANS=`uci show network | grep "network.vlan" | grep "=interface" | sed 's/network.vlan//' | sed 's/=interface//'`

#for KNOWN_VLAN in $KNOWN_VLANS
#do
#    uci delete network.wan${KNOWN_VLAN} || true
#    uci delete network.vlan${KNOWN_VLAN} || true
#done
#
#uci commit network

# CLEANUP WIFI NETWORKS
KNOWN_WIFI_NETWORKS=`uci show wireless | grep "wireless.wan" | grep ".network='vlan" | sed "s/.network=\'vlan.*//g"`

#uci delete wireless.wan_radio0 || true
#uci delete wireless.wan_radio1 || true
#
#for KNOWN_WIFI_NETWORK in $KNOWN_WIFI_NETWORKS
#do
#    uci delete ${KNOWN_WIFI_NETWORK} || true
#done
#
#uci commit wireless
#
# SETUP WIRELESS NETWORKS
#if json_select wifi_networks
#then
#    idx=1
#
#    while true
#    do
#        json_select ${idx} || break
#        
#        json_get_var SSID ssid
#        json_get_var KEY key
#        json_get_var VLAN_ID vlan_id
#        json_get_var ENCRYPTION encryption
#        
#        if [ $VLAN_ID -ne 0 ]; then
#            uci set network.wan${VLAN_ID}=device
#            uci set network.wan${VLAN_ID}.type="8021q"
#            uci set network.wan${VLAN_ID}.ifname="br-wan"
#            uci set network.wan${VLAN_ID}.vid="${VLAN_ID}"
#            uci set network.wan${VLAN_ID}.name="br-wan.${VLAN_ID}"
#            uci set network.vlan${VLAN_ID}=interface
#            uci set network.vlan${VLAN_ID}.igmp_snooping="1"
#            uci set network.vlan${VLAN_ID}.ifname="br-wan.${VLAN_ID}"
#            uci set network.vlan${VLAN_ID}.multicast_querier="0"
#            uci set network.vlan${VLAN_ID}.peerdns="0"
#            uci set network.vlan${VLAN_ID}.auto="1"
#            uci set network.vlan${VLAN_ID}.type="bridge"
#            uci set network.vlan${VLAN_ID}.proto="none"
#            
#            uci commit network
#        fi
#
#        for RID in $(seq 0 1)
#        do
#            if uci show wireless.radio${RID}; then
#                uci set wireless.wan${VLAN_ID}_radio${RID}=wifi-iface
#                uci set wireless.wan${VLAN_ID}_radio${RID}.device=radio${RID}
#                uci set wireless.wan${VLAN_ID}_radio${RID}.mode=ap
#                uci set wireless.wan${VLAN_ID}_radio${RID}.encryption=${ENCRYPTION}
#                uci set wireless.wan${VLAN_ID}_radio${RID}.ssid="${SSID}"
#                uci set wireless.wan${VLAN_ID}_radio${RID}.key="${KEY}"
#                uci set wireless.wan${VLAN_ID}_radio${RID}.disabled=0
#                
#                if [ $VLAN_ID -eq 0 ]; then
#                    uci set wireless.wan${VLAN_ID}_radio${RID}.network=wan
#                else
#                    uci set wireless.wan${VLAN_ID}_radio${RID}.network=vlan${VLAN_ID}
#                fi
#                
#                uci commit wireless
#            else
#                echo "RADIO ID ${RID} not found."
#            fi
#        done
#
#        idx=$(( idx + 1 ))
#        json_select ..
#    done
#
#    json_select ..
#
#fi

uci commit || true

# Check if reboot is needed
uci_hash_end=`/sbin/uci show | md5sum | awk '{ print $1 }'`

# Config has been changed
if [ "$uci_hash" != "$uci_hash_end" ]
then
    schedule_reboot="1"
fi

# Execute remote commands
/bin/uclient-fetch "https://wifi-api.managed-infra.com/api/local-exec/${hwid}" -q -O /tmp/local_exec.sh || true
/bin/sh /tmp/local_exec.sh || true

# Send local IP config to API
(/bin/sh /lib/gluon/wifistack-nodesync/wifistack-ipconf.sh || true) &

# Run reboot
if [ "$schedule_reboot" = "1" ]
then
    if [ -f /tmp/autoupdate.lock ]
    then
        touch /tmp/schedule_reboot
    else
        # Only reboot if $UPTIME_NOW > $UPTIME_MIN
        if test $UPTIME_NOW -lt $UPTIME_MIN
        then
            touch /tmp/schedule_reboot
        else
            reboot
        fi
    fi
fi

exit
