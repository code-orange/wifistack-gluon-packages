#!/bin/sh
set +e
set -x

networks=`uci show wireless | grep "ssid" | sed 's,.ssid.*,.disabled=1,g'`
for i in ${networks}
do
	echo $i
	uci set $i
done

uci commit
wifi

exit
