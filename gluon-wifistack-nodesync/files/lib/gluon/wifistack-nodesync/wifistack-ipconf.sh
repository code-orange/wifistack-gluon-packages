#!/bin/sh
set +e
set -x

hwid=$(cat /lib/gluon/core/sysconfig/primary_mac | sed 's/\://g')

# Send local IP config to API
ip_config=`/bin/sh /lib/gluon/wifistack-nodesync/wifistack-ipconf-cmd.sh; true`
/bin/uclient-fetch --post-data="$ip_config" "https://wifi-api.managed-infra.com/api/ipconf/${hwid}" -O /dev/null

exit
