#!/bin/sh
set +e
set -x

/sbin/ip a
/usr/sbin/lldpctl
/usr/bin/iwinfo
/bin/traceroute 8.8.8.8
/bin/traceroute 2001:4860:4860::8888

exit 0
