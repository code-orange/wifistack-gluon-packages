# gluon-wifistack-nodesync

Sync node information with what is stored in the master NodeDB.

Data on the nodes is generally considered as volatile.
The NodeDB has general precedence and overrides device values
on a regular basis.
