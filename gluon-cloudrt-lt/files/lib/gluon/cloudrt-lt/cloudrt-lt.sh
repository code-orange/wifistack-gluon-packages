#!/bin/sh
set -e
set -x

. /usr/share/libubox/jshn.sh

UPTIME_NOW=$(cut -d '.' -f1 /proc/uptime)
UPTIME_MIN=180

# Wait for up to 99 seconds until execution
sleep $(head -30 /dev/urandom | tr -dc "0123456789" | head -c2)

uci_hash=`/sbin/uci show | md5sum | awk '{ print $1 }'`

schedule_reboot="0"

if [ -f /tmp/schedule_reboot ]
then
    schedule_reboot="1"
fi

# Allow VPN interfaces in firewall
uci set network.custom_vpn0=interface
uci set network.custom_vpn0.ifname='tun0'
uci set network.custom_vpn0.proto='dhcp'

uci set network.custom_vpn1=interface
uci set network.custom_vpn1.ifname='tun1'
uci set network.custom_vpn1.proto='dhcp'

uci set firewall.@zone[6]=zone
uci set firewall.@zone[6].name='custom_vpn0'
uci set firewall.@zone[6].network='custom_vpn0'
uci set firewall.@zone[6].input='ACCEPT'
uci set firewall.@zone[6].output='ACCEPT'
uci set firewall.@zone[6].forward='ACCEPT'
uci set firewall.@zone[6].masq='0'
uci set firewall.@zone[6].mtu_fix='1'

uci set firewall.@zone[7]=zone
uci set firewall.@zone[7].name='custom_vpn1'
uci set firewall.@zone[7].network='custom_vpn1'
uci set firewall.@zone[7].input='ACCEPT'
uci set firewall.@zone[7].output='ACCEPT'
uci set firewall.@zone[7].forward='ACCEPT'
uci set firewall.@zone[7].masq='0'
uci set firewall.@zone[7].mtu_fix='1'

uci set firewall.@forwarding[1]=forwarding
uci set firewall.@forwarding[1].src='custom_vpn0'
uci set firewall.@forwarding[1].dest='wan'

uci set firewall.@forwarding[2]=forwarding
uci set firewall.@forwarding[2].src='custom_vpn1'
uci set firewall.@forwarding[2].dest='wan'

uci set firewall.@forwarding[3]=forwarding
uci set firewall.@forwarding[3].src='wan'
uci set firewall.@forwarding[3].dest='custom_vpn0'

uci set firewall.@forwarding[4]=forwarding
uci set firewall.@forwarding[4].src='wan'
uci set firewall.@forwarding[4].dest='custom_vpn1'

uci commit

# Setup OpenVPN
uci set openvpn.custom_vpn0=openvpn
uci set openvpn.custom_vpn0.nobind='1'
uci set openvpn.custom_vpn0.float='1'
uci set openvpn.custom_vpn0.client='1'
uci set openvpn.custom_vpn0.comp_lzo='yes'
uci set openvpn.custom_vpn0.reneg_sec='0'
uci set openvpn.custom_vpn0.dev='tun0'
uci set openvpn.custom_vpn0.verb='3'
uci set openvpn.custom_vpn0.persist_tun='1'
uci set openvpn.custom_vpn0.persist_key='1'
uci set openvpn.custom_vpn0.remote_cert_tls='server'
uci set openvpn.custom_vpn0.remote='185.118.198.140'
uci set openvpn.custom_vpn0.ca='/etc/luci-uploads/cbid.openvpn.custom_vpn0.ca'
uci set openvpn.custom_vpn0.cert='/etc/luci-uploads/cbid.openvpn.custom_vpn0.cert'
uci set openvpn.custom_vpn0.key='/etc/luci-uploads/cbid.openvpn.custom_vpn0.key'
uci set openvpn.custom_vpn0.dev_type='tun'
uci set openvpn.custom_vpn0.enabled='1'
uci set openvpn.custom_vpn0.pull='1'
uci set openvpn.custom_vpn0.remote_random='1'
uci set openvpn.custom_vpn0.cipher='AES-256-CBC'
uci set openvpn.custom_vpn0.keepalive='10 60'
uci set openvpn.custom_vpn0.port='4445'

uci set openvpn.custom_vpn1=openvpn
uci set openvpn.custom_vpn1.nobind='1'
uci set openvpn.custom_vpn1.float='1'
uci set openvpn.custom_vpn1.client='1'
uci set openvpn.custom_vpn1.comp_lzo='yes'
uci set openvpn.custom_vpn1.reneg_sec='0'
uci set openvpn.custom_vpn1.dev='tun1'
uci set openvpn.custom_vpn1.verb='3'
uci set openvpn.custom_vpn1.persist_tun='1'
uci set openvpn.custom_vpn1.persist_key='1'
uci set openvpn.custom_vpn1.remote_cert_tls='server'
uci set openvpn.custom_vpn1.remote='185.118.198.140'
uci set openvpn.custom_vpn1.ca='/etc/luci-uploads/cbid.openvpn.custom_vpn1.ca'
uci set openvpn.custom_vpn1.cert='/etc/luci-uploads/cbid.openvpn.custom_vpn1.cert'
uci set openvpn.custom_vpn1.key='/etc/luci-uploads/cbid.openvpn.custom_vpn1.key'
uci set openvpn.custom_vpn1.dev_type='tun'
uci set openvpn.custom_vpn1.enabled='1'
uci set openvpn.custom_vpn1.pull='1'
uci set openvpn.custom_vpn1.remote_random='1'
uci set openvpn.custom_vpn1.cipher='AES-256-CBC'
uci set openvpn.custom_vpn1.keepalive='10 60'
uci set openvpn.custom_vpn1.port='4445'

uci commit

# Check if reboot is needed
uci_hash_end=`/sbin/uci show | md5sum | awk '{ print $1 }'`

# Config has been changed
if [ "$uci_hash" != "$uci_hash_end" ]
then
    schedule_reboot="1"
fi

# Run reboot
if [ "$schedule_reboot" = "1" ]
then
    touch /tmp/schedule_reboot
fi

exit
