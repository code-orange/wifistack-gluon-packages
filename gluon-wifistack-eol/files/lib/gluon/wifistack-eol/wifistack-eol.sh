#!/bin/sh
set -e
set -x

my_model=`lua -e 'print(require("platform_info").get_image_name())'`

while read dev; do
	if [ $dev == $my_model ]
	then
		echo "$dev is EOL"
		true > /etc/config/wireless
		uci set gluon.core.domain="suspended_hard"
		gluon-reconfigure
		reboot
	fi
done </lib/gluon/wifistack-eol/wifistack-eol-devices.txt

exit
