#!/bin/sh
set -e
set -x

IP6GATEWAY=`/sbin/ip -6 route | grep ^default |awk '/default/ { print $3 }'`

/usr/bin/wget -6 --no-check-certificate https://[${IP6GATEWAY}%br-client]/wifi-node -O /dev/null -T 10
/usr/bin/wget -6 --no-check-certificate http://captive.apple.com/wifi-node -O /dev/null -T 10

exit
